import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class amazonteste {
    private WebDriver driver;

    @Before
    public void abrir(){
        System.setProperty("webdriver.gecko.driver","C:/Drive/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com.br/");



        }
        @After
        public void sair(){
            driver.quit();

    }
    @Test
    public void maisVendidos() throws InterruptedException{
        Thread.sleep(3000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("/html/body/div[3]/div[2]/div/ul[1]/li[2]/a")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Mais vendidos", driver.findElement(By.xpath("//*[@id=\"zg_banner_text\"]")).getText());
        Thread.sleep(3000);
    }
    @Test
        public void testarBuscador() throws InterruptedException {
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Qualquer");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Thread.sleep(300);
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());
        Thread.sleep(300);

    }
    @Test
        public void testarAltenticadorSenhaIncorreta() throws InterruptedException {
        driver.findElement(By.id("nav-link-accountList")).click();
        driver.findElement(By.id("ap_email")).sendKeys("blabla70001@hotmail.com");
        driver.findElement(By.id("continue")).click();
        Assert.assertEquals("Não encontramos uma conta associada a este endereço de e-mail", driver.findElement(By.className("a-list-item")).getText());
        Thread.sleep(3000);
    }
    @Test
        public void testarLivro() throws InterruptedException{
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"nav-xshop\"]/a[6]")).click();
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Java");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[1]/div/span[3]/div[2]/div[1]/div/span/div/div/span")).getText());


    }

    @Test
        public void testeAbaTodos()throws InterruptedException{
        Thread.sleep(3000);
        driver.findElement(By.id("nav-hamburger-menu")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"hmenu-content\"]/ul[1]/li[3]/a")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Novidades na Amazon", driver.findElement(By.id("zg_banner_text")).getText());
    }

    @Test
        public void carrinhoCompras() throws InterruptedException{
        driver.findElement(By.id("nav-cart")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"a-autoid-1-announce\"]/span")).click();
        Thread.sleep(3000);
        Assert.assertEquals("Criar conta",driver.findElement(By.xpath("//*[@id=\"ap_register_form\"]/div/div/h1")).getText() );
    }
    @Test
        public void atendimentoAoCliente()throws InterruptedException{
        Thread.sleep(3000);
        driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Computador");
        driver.findElement(By.id("nav-search-submit-button")).click();
        Assert.assertEquals("RESULTADOS", driver.findElement(By.xpath("//*[@id=\"search\"]/div[1]/div[1]/div/span[3]/div[2]/div[2]/div/span/div/div/span")).getText());
    }




}
